#include "DictionaryList.h"
typedef std::string Data;
int main()
{
	setlocale(LC_ALL, "Russian");
	DictionaryList<Data> dictionary[10];
	printMenu();
	int choice = 0;
	int versionOfDictionary = 0;
	int versionOfDictionarySecond = 0;
	int versionOfDictionaryThird = 0;
	std::string word = "";
	std::cin >> choice;
	while (std::cin.peek() == '\n' && choice >= 1 && choice <= 8)
	{
		try
		{
			std::cout << "�������� ����� ������� �� 0 �� 9\n";
			std::cin >> versionOfDictionary;
			if (std::cin.peek() != '\n' || versionOfDictionary < 0 || versionOfDictionary > 9)
				throw "������� �� ��������\n";
			switch (choice)
			{
			case 1:
				std::cout << "������� �����\n";
				std::cin >> word;
				if (!isCorrectWord(word))
					throw "�� ����� �� �����. ����� ������ �������� �� ����\n";
				dictionary[versionOfDictionary].insertNode(word);
				std::cout << "����� �������\n";
				break;
			case 2:
				std::cout << "������� �����\n";
				std::cin >> word;
				if (!isCorrectWord(word))
					throw "�� ����� �� �����. ����� ������ �������� �� ����\n";
				dictionary[versionOfDictionary].deleteNode(word);
				std::cout << "����� �������\n";
				break;
			case 3:
				std::cout << "������� �����\n";
				std::cin >> word;
				if (!isCorrectWord(word))
					throw "�� ����� �� �����. ����� ������ �������� �� ����\n";
				if (dictionary[versionOfDictionary].searchForNode(word))
					std::cout << "����� " << word << " ������� � ������� " << versionOfDictionary << std::endl;
				else
					std::cout << "����� " << word << " ����������� � ������� " << versionOfDictionary << std::endl;
				break;
			case 4:
				dictionary[versionOfDictionary].printList();
				break;
			case 5:
				std::cout << "�������� ����� ������� ������� �� 0 �� 9, �������� ������ �������� ����� �������\n";
				std::cin >> versionOfDictionarySecond;
				if (std::cin.peek() != '\n' || versionOfDictionarySecond < 0 || versionOfDictionarySecond > 9 || versionOfDictionarySecond == versionOfDictionary)
					throw "������ ������� �� ��������\n";
				dictionary[versionOfDictionary].merge(&dictionary[versionOfDictionarySecond]);
				std::cout << "������� ����������\n";
				break;
			case 6:
				std::cout << "�������� ����� ������� ������� �� 0 �� 9, �������� ������ �������� ����� �������\n";
				std::cin >> versionOfDictionarySecond;
				if (std::cin.peek() != '\n' || versionOfDictionarySecond < 0 || versionOfDictionarySecond > 9 || versionOfDictionarySecond == versionOfDictionary)
					throw "������ ������� �� ��������\n";
				dictionary[versionOfDictionary].deleteWords(&dictionary[versionOfDictionarySecond]);
				std::cout << "������� �����\n";
				break;
			case 7:
				std::cout << "�������� ����� ������� ������� �� 0 �� 9, �������� ������ �������� ����� �������\n";
				std::cin >> versionOfDictionarySecond;
				if (std::cin.peek() != '\n' || versionOfDictionarySecond < 0 || versionOfDictionarySecond > 9 || versionOfDictionarySecond == versionOfDictionary)
					throw "������ ������� �� ��������\n";
				std::cout << "�������� ����� �������� ������� �� 0 �� 9, �������� ������ � ������ �������� ������ ��������\n";
				std::cin >> versionOfDictionaryThird;
				if (std::cin.peek() != '\n' || versionOfDictionaryThird < 0 || versionOfDictionaryThird > 9 || versionOfDictionaryThird == versionOfDictionary || versionOfDictionaryThird == versionOfDictionarySecond)
					throw "������ ������� �� ��������\n";
				dictionary[versionOfDictionaryThird] = DictionaryList<Data>(getIntersection(&dictionary[versionOfDictionary], &dictionary[versionOfDictionarySecond]));
				std::cout << "����������� ��������\n";
				break;
			case 8:
				dictionary[versionOfDictionary] = DictionaryList<Data>();
				std::cout << "������ ����� �������. ������ ������� ��� ���� ������� �����\n";
				break;
			}
			printMenu();
			std::cin >> choice;
		}
		catch (const char* e)
		{
			std::cerr << e << std::endl;
			printMenu();
			std::cin >> choice;
		}

	}
	std::cout << "�� ��������� ������ ���������";
	return 0;
}